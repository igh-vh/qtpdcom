# General Information

This is the QtPdCom library, a Qt implementation of PdCom 5
from the EtherLab project. See https://gitlab.com/etherlab.org/qtpdcom

# License

The QtPdCom library is licensed unter the terms and conditions of the GNU
Lesser General Public License (LGPL), version 3 or (at your option) any later
version.

# Prerequisites

- Qt5
- PdCom 5 (see https://gitlab.com/etherlab.org/pdcom)

# Building and Installing

## qmake
The build process is handled by qmake. To build and install, call:

```
qmake
make
make install
```

## CMake

CMake is also able to build this library.

```
mkdir build && cd build
cmake ..
make install
```

To change the installation path, call cmake with `-DCMAKE_INSTALL_PREFIX=/my/custom/prefix`.
If PdCom5 is installed at a non-standard location,
use `-DCMAKE_PREFIX_PATH=/pdcom/install/prefix`.
You can even use this option to point CMake to a build directory of PdCom5,
this adds the possibility to use a version of PdCom5 which is not installed at all.

To import this library into your own application,
add the following to your `CMakeLists.txt`:
```cmake
find_package(QtPdCom1 REQUIRED)
target_link_libraries(your_application PUBLIC
   EtherLab::QtPdCom1
)
```
All dependencies are imported and linked automatically.
The `CMAKE_PREFIX_PATH` argument works here, too.

Have fun!
