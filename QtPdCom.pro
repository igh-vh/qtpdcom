#-----------------------------------------------------------------------------
#
# vim: syntax=config
#
# Copyright (C) 2012-2022  Florian Pose <fp@igh.de>
#
# This file is part of the QtPdCom library.
#
# The QtPdCom library is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# The QtPdCom library is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License
# for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with the QtPdCom Library. If not, see <http://www.gnu.org/licenses/>.
#
#-----------------------------------------------------------------------------

TEMPLATE = lib
TARGET = QtPdCom

# On release:
# - Change version in Doxyfile
# - Add a NEWS entry
VERSION = 1.0.0

MAJOR = $$section(VERSION, ., 0, 0)
TARGET_MAJOR = $${TARGET}$${MAJOR} # with major number
BASENAME = $$TARGET # without major number

unix {
    TARGET = $${TARGET_MAJOR}
}

CONFIG += debug_and_release hide_symbols c++14

DEFINES += $${TARGET_MAJOR}_EXPORTS

QT += network xml

# Path definitions (can be overridden via qmake command-line) ----------------

# Installation prefix (used for header installation only)
isEmpty(PD_INSTALL_PREFIX) {
    PD_INSTALL_PREFIX = $$[QT_INSTALL_PREFIX]
}

# Library installation path
isEmpty(PD_INSTALL_LIBS) {
    PD_INSTALL_LIBS = $$[QT_INSTALL_LIBS]
}

# DLL installation path
isEmpty(PD_INSTALL_BINS) {
    PD_INSTALL_BINS = $$[QT_INSTALL_BINS]
}

!isEmpty(PD_LIB_PREFIX) {
    INCLUDEPATH += $${PD_LIB_PREFIX}/include
    LIBS += -L$${PD_LIB_PREFIX}/lib
}

# Compilation flags and directories ------------------------------------------

DEPENDPATH += .
MOC_DIR = .moc
OBJECTS_DIR = .obj

INCLUDEPATH += $${PWD}/$${TARGET_MAJOR}

LIBS += -lpdcom5

win32 {
    LIBS += -lwsock32 # for gethostname in Process.cpp
    CONFIG += dll
}

# Install libraries and DLLs -------------------------------------------------

libraries.path = $$PD_INSTALL_LIBS

unix { # also android
    libraries.files = $${OUT_PWD}/lib$${TARGET_MAJOR}.so
    INSTALLS += libraries
}

win32 {
    CONFIG(release, debug|release):DIR = "release"
    CONFIG(debug, debug|release):DIR = "debug"

    libraries.files = "$${OUT_PWD}/$${DIR}/lib$${TARGET_MAJOR}.dll.a"

    dlls.path = $$PD_INSTALL_BINS
    dlls.files = "$${OUT_PWD}/$${DIR}/$${TARGET_MAJOR}.dll"

    INSTALLS += dlls libraries
}

# install headers ------------------------------------------------------------

inst_headers.path = $${PD_INSTALL_PREFIX}/include/$${TARGET_MAJOR}

inst_headers.files = \
    $${TARGET_MAJOR}/Export.h \
    $${TARGET_MAJOR}/Message.h \
    $${TARGET_MAJOR}/MessageModel.h \
    $${TARGET_MAJOR}/Process.h \
    $${TARGET_MAJOR}/ScalarSubscriber.h \
    $${TARGET_MAJOR}/ScalarVariable.h \
    $${TARGET_MAJOR}/TableColumn.h \
    $${TARGET_MAJOR}/TableModel.h \
    $${TARGET_MAJOR}/Translator.h \
    $${TARGET_MAJOR}/Transmission.h

INSTALLS += inst_headers

# CMake config for find_package(qtpdcom) -------------------------------------

QMAKE_SUBSTITUTES += qtpdcom-config.cmake.in

cmake_files.path = $$PD_INSTALL_LIBS/cmake/$$lower($${TARGET_MAJOR})
cmake_files.files = qtpdcom-config.cmake

INSTALLS += cmake_files

# define headers and sources -------------------------------------------------

HEADERS += \
    $${inst_headers.files} \
    src/MessageImpl.h \
    src/MessageItem.h \
    src/MessageManager.h \
    src/MessageModelImpl.h

SOURCES += \
    src/Message.cpp \
    src/MessageImpl.cpp \
    src/MessageItem.cpp \
    src/MessageManager.cpp \
    src/MessageModel.cpp \
    src/MessageModelImpl.cpp \
    src/Process.cpp \
    src/ScalarSubscriber.cpp \
    src/TableColumn.cpp \
    src/TableModel.cpp \
    src/Translator.cpp \
    src/Transmission.cpp

# translations ---------------------------------------------------------------

CONFIG += lrelease embed_translations
TRANSLATIONS = \
    QtPdCom_de.ts \
    QtPdCom_nl.ts

QM_FILES_RESOURCE_PREFIX=/QtPdCom
# make qmake to name the translation resource file the same as with cmake
# this is needed because Q_INIT_RESOURCE needs to be called
QMAKE_RESOURCE_FLAGS = -name QtPdCom_ts
CODECFORTR = UTF-8

# additional files to install ------------------------------------------------

ADDITIONAL_DISTFILES = \
    AUTHORS \
    COPYING \
    COPYING.LESSER \
    Doxyfile \
    NEWS \
    images/* \
    README

DISTFILES += $${ADDITIONAL_DISTFILES}

#-----------------------------------------------------------------------------
