/*****************************************************************************
 *
 * Copyright (C) 2009-2022  Florian Pose <fp@igh.de>
 *
 * This file is part of the QtPdCom library.
 *
 * The QtPdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The QtPdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdCom Library. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef PD_MESSAGE_IMPL
#define PD_MESSAGE_IMPL

#include "Message.h"
#include "MessageModelImpl.h"
#include "ScalarVariable.h"

#include <pdcom5/MessageManagerBase.h>

#include <QObject>
#include <QDomElement>
#include <QMap>

/****************************************************************************/

namespace QtPdCom {

class Message::Impl:
    public QObject
{
    Q_OBJECT

    friend class Message;
    friend class MessageModel;

    public:
        Impl(Message *);
        ~Impl();

        static QString pathFromPlainXmlElement(QDomElement,
                const QString & = QString());
        static int indexFromPlainXmlElement(QDomElement);

        void fromPlainXmlElement(QDomElement, const QString & = QString());
        void fromPdComMessage(const PdCom::Message &);

        static QString timeString(quint64);

    private:
        Message * const parent;

        Type type; /**< Message type. */
        QString path; /**< Path of the process variable. */
        int index;
        typedef QMap<QString, QString> TranslationMap;
        TranslationMap text; /**< Text of the message. */
        TranslationMap description; /**< Description of the message. */

        DoubleVariable variable;
        MessageModel::Impl::MessageItem *currentItem;

        void loadTranslations(QDomElement, TranslationMap &);

        static Message::Type typeFromString(const QString &);

    private slots:
        void valueChanged();
};

} // namespace

#endif

/****************************************************************************/
