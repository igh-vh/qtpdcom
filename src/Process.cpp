/*****************************************************************************
 *
 * Copyright (C) 2009-2022  Florian Pose <fp@igh.de>
 *
 * This file is part of the QtPdCom library.
 *
 * The QtPdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The QtPdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdCom Library. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "Process.h"
#include "MessageManager.h"

#include <pdcom5/MessageManagerBase.h>

#ifdef __WIN32__
#include <windows.h> // GetUserName(), gethostname()
#include <lmcons.h> // UNLEN
#else
#include <unistd.h> // getlogin()
#endif

#include <string>
#include <list>

#include <QTranslator>

#define DEBUG_DATA 0

using QtPdCom::Process;

/****************************************************************************/

struct Process::Impl
{
    Impl(Process *process):
        process(process),
        messageManager(),
        appName("QtPdCom1"),
        socketValid(false),
        connectionState(Disconnected),
        rxBytes(0),
        txBytes(0)
    {
        // set the default process to the always last instance of process
        defaultProcess = process;
    }

    Process * const process;

    QtPdCom::MessageManager messageManager;

    QString appName; /**< Our application name, that is announced to the
                       server. Default: QtPdCom1.  */
    QTcpSocket socket; /**< TCP socket to the process. */
    bool socketValid; /**< Connection state of the socket. */
    ConnectionState connectionState; /**< The current connection state. */
    QString errorString; /**< Error reason. Set, before error() is
                           emitted. */
    quint64 rxBytes;
    quint64 txBytes;

    static QtPdCom::Process *defaultProcess; /**< last created process
                                          is the default process */
};

/****************************************************************************/

QtPdCom::Process *QtPdCom::Process::Impl::defaultProcess = nullptr;

/*****************************************************************************
 * Public implementation
 ****************************************************************************/

/** Constructor.
 */
Process::Process(QObject *parent):
    QObject(parent),
    PdCom::Process(),
    impl{std::unique_ptr<Impl>{new Impl{this}}}
{
    connect(&impl->socket, SIGNAL(connected()),
            this, SLOT(socketConnected()));
    connect(&impl->socket, SIGNAL(disconnected()),
            this, SLOT(socketDisconnected()));
    connect(&impl->socket, SIGNAL(error(QAbstractSocket::SocketError)),
            this, SLOT(socketError()));
    connect(&impl->socket, SIGNAL(readyRead()),
            this, SLOT(socketRead()));

    setMessageManager(&impl->messageManager);
}

/****************************************************************************/

/** Destructor.
 */
Process::~Process()
{
    setMessageManager(nullptr);

    // reset default process if this instance is the default
    if (impl->defaultProcess == this) {
        impl->defaultProcess = nullptr;
    }

    disconnectFromHost();
}

/****************************************************************************/

/** Sets the application name.
 */
void Process::setApplicationName(const QString &name)
{
    impl->appName = name;
}

/****************************************************************************/

/** Starts to connect to a process.
 */
void Process::connectToHost(const QString &address, quint16 port)
{
    impl->connectionState = Connecting;
    impl->socket.connectToHost(address, port);
}

/****************************************************************************/

/** Disconnects from a process.
 */
void Process::disconnectFromHost()
{
    switch (impl->connectionState) {
        case Connecting:
        case Connected:
            impl->socketValid = false;
            impl->connectionState = Disconnected;
            impl->rxBytes = 0;
            impl->txBytes = 0;
            reset();
            impl->socket.disconnectFromHost();
            emit disconnected();
            break;

        default:
            break;
    }
}

/****************************************************************************/

/**
 * \return The #connectionState.
 */
Process::ConnectionState Process::getConnectionState() const
{
    return impl->connectionState;
}

/****************************************************************************/

/**
 * \return \a true, if the process is connected.
 */
bool Process::isConnected() const
{
    return impl->connectionState == Connected;
}

/****************************************************************************/

/**
 * \return Error reason after the error() signal was emitted.
 */
const QString &Process::getErrorString() const
{
    return impl->errorString;
}

/****************************************************************************/

/**
 * \return Host name of the process.
 */
QString Process::getPeerName() const
{
    return impl->socket.peerName();
}

/****************************************************************************/

/** Wrapper function for Process::findVariable.
 */
void Process::find(const QString &path)
{
    PdCom::Process::find(path.toLocal8Bit().constData());
}

/****************************************************************************/

/** Send a broadcast message.
 */
void Process::sendBroadcast(const QString &msg, const QString &attr)
{
    broadcast(msg.toLocal8Bit().constData(),
            attr.toLocal8Bit().constData());
}

/****************************************************************************/

quint64 Process::getRxBytes() const
{
    return impl->rxBytes;
}

/****************************************************************************/

quint64 Process::getTxBytes() const
{
    return impl->txBytes;
}

/****************************************************************************/

QtPdCom::Process *Process::getDefaultProcess()
{
    return QtPdCom::Process::Impl::defaultProcess;
}

/****************************************************************************/

/** Set default process "manually"
 */
void Process::setDefaultProcess(QtPdCom::Process *process)
{
    QtPdCom::Process::Impl::defaultProcess = process;
}

/****************************************************************************/

PdCom::MessageManagerBase *Process::getMessageManager() const
{
    return &impl->messageManager;
}

/*****************************************************************************
 * private methods
 ****************************************************************************/

std::string Process::applicationName() const
{
    return impl->appName.toLocal8Bit().constData();
}

/****************************************************************************/

/** Read data from the socket.
 */
int Process::read(char *buf, int count)
{
    qint64 ret(impl->socket.read(buf, count));
    if (ret > 0) {
        impl->rxBytes += ret;
    }
    return ret;
}

/****************************************************************************/

/** Sends data via the socket.
 *
 * This is the implementation of the virtual PdCom::Process
 * method to enable the Process object to send data to the process.
 */
void Process::write(const char *buf, size_t count)
{
#if DEBUG_DATA
    qDebug() << "Writing:" << count << QByteArray(buf, count);
#endif
    while (count > 0) {
        qint64 ret(impl->socket.write(buf, count));
        if (ret <= 0) {
            qWarning("write() failed.");
            impl->socketValid = false;
            impl->rxBytes = 0;
            impl->txBytes = 0;
            reset();
            impl->socket.disconnectFromHost();
            emit error();
            return;
        }
        count -= ret;
        buf += ret;
        impl->txBytes += ret;
    }
}

/****************************************************************************/

/** Flushed the socket.
 */
void Process::flush()
{
#if DEBUG_DATA
    qDebug() << "Flushing not implemented.";
#endif
}

/****************************************************************************/

/** The process is connected and ready.
 *
 * This virtual function from PdCom::Process has to be overloaded to let
 * subclasses know about this event.
 */
void Process::connected()
{
    impl->connectionState = Connected;
    emit processConnected();

    // query messages
    impl->messageManager.activeMessages();
}

/****************************************************************************/

/** Broadcast Reply.
 */
void Process::broadcastReply(
        const std::string &message,
        const std::string &attr,
        std::chrono::nanoseconds time_ns,
        const std::string &user)

{
    emit broadcastReceived(
            QString::fromStdString(message),
            QString::fromStdString(attr),
            time_ns.count(),
            QString::fromStdString(user));
}

/****************************************************************************/

/** Ping Reply.
 */
void Process::pingReply()
{
    emit pingReceived();
}

/****************************************************************************/

/** Resets the PdCom process.
 */
void Process::reset()
{
    try {
        PdCom::Process::reset();
    } catch (std::exception &e) {
        // do nothing
    }

    impl->messageManager.reset();
}

/****************************************************************************/

/** Socket connection established.
 *
 * This is called, when the pure socket connection was established and the
 * Process object can start using it.
 */
void Process::socketConnected()
{
    impl->socketValid = true;
    impl->socket.setSocketOption(QAbstractSocket::KeepAliveOption, 1);
}

/****************************************************************************/

/** Socket disconnected.
 *
 * The socket was closed and the process has to be told, that it is
 * disconnected.
 */
void Process::socketDisconnected()
{
    switch (impl->connectionState) {
        case Connecting:
            impl->socketValid = false;
            impl->connectionState = ConnectError;
            impl->rxBytes = 0;
            impl->txBytes = 0;
            reset();
            emit error();
            break;

        case Connected:
            impl->socketValid = false;
            impl->connectionState = Disconnected;
            impl->rxBytes = 0;
            impl->txBytes = 0;
            reset();
            emit disconnected();
            break;

        default:
            break;
    }
}

/****************************************************************************/

/** There was a socket error.
 *
 * The error could come up either while connecting the socket, or when the
 * socket was already connected.
 */
void Process::socketError()
{
    impl->errorString = impl->socket.errorString();

    switch (impl->connectionState) {
        case Connecting:
            impl->socketValid = false;
            impl->connectionState = ConnectError;
            impl->rxBytes = 0;
            impl->txBytes = 0;
            reset();
            emit error();
            break;

        case Connected:
            impl->socketValid = false;
            impl->connectionState = ConnectedError;
            impl->rxBytes = 0;
            impl->txBytes = 0;
            reset();
            emit error();
            break;

        default:
            break;
    }
}

/****************************************************************************/

/** The socket has new data to read.
 */
void Process::socketRead()
{
    try {
        while (impl->socket.bytesAvailable() > 0) {
            asyncData();
        }
    }
    catch (std::exception &e) {
        impl->errorString = "Exception during asyncData(): ";
        impl->errorString += e.what();
        qCritical() << impl->errorString;
        impl->socketValid = false;
        if (impl->connectionState == Connected) {
            impl->connectionState = ConnectedError;
        } else {
            impl->connectionState = ConnectError;
        }
        reset();
        impl->socket.disconnectFromHost();
        emit error();
    }
    catch (...) {
        impl->errorString = "Unknown exception during asyncData()";
        qCritical() << impl->errorString;
        impl->socketValid = false;
        if (impl->connectionState == Connected) {
            impl->connectionState = ConnectedError;
        } else {
            impl->connectionState = ConnectError;
        }
        reset();
        impl->socket.disconnectFromHost();
        emit error();
    }
}

/****************************************************************************/

static void init_resources() {
    // needs to be at global namespace
    Q_INIT_RESOURCE(QtPdCom_ts);
}

bool Process::loadTranslations(QTranslator& translator, const QString &locale)
{
    init_resources();

    return translator.load(":/QtPdCom/QtPdCom_"+locale);
}
