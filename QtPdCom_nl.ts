<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="nl_NL" sourcelanguage="en">
<context>
    <name>Pd::MessageModel</name>
    <message>
        <source>Failed to open %1.</source>
        <translation type="obsolete">Openen van %1 mislukt.</translation>
    </message>
    <message>
        <source>Failed to parse %1, line %2, column %3: %4</source>
        <translation type="obsolete">Parsen van %1, lijn %2, kolom %3 mislukt: %4</translation>
    </message>
    <message>
        <source>Message variable %1 not found!</source>
        <translation type="obsolete">Kon meldingsvariabele %1 niet vinden.</translation>
    </message>
    <message>
        <source>Failed to subscribe to %1: %2</source>
        <translation type="obsolete">Kon geen abonnement nemen op %1: %2</translation>
    </message>
    <message>
        <source>Message</source>
        <translation type="obsolete">Melding</translation>
    </message>
    <message>
        <source>Time</source>
        <translation type="obsolete">Tijd</translation>
    </message>
</context>
<context>
    <name>QtPdCom::MessageModel</name>
    <message>
        <location filename="src/MessageModel.cpp" line="71"/>
        <source>Failed to open %1.</source>
        <translation>Openen van %1 mislukt.</translation>
    </message>
    <message>
        <location filename="src/MessageModel.cpp" line="77"/>
        <source>Failed to parse %1, line %2, column %3: %4</source>
        <translation>Parsen van %1, lijn %2, kolom %3 mislukt: %4</translation>
    </message>
    <message>
        <location filename="src/MessageModel.cpp" line="88"/>
        <source>Failed to process %1: No plain message file (%2)!</source>
        <translation>Verwerken van %1 mislukt: Geen vlakke meldingsfile gevonden (%2)!</translation>
    </message>
    <message>
        <location filename="src/MessageModel.cpp" line="193"/>
        <source>Failed to connect to message manager.</source>
        <translation>Verbinding met meldings manager mislukt.</translation>
    </message>
    <message>
        <location filename="src/MessageModel.cpp" line="207"/>
        <source>Failed to subscribe to %1: %2</source>
        <translation>Kon geen abonnement nemen op %1: %2</translation>
    </message>
    <message>
        <location filename="src/MessageModel.cpp" line="379"/>
        <source>Message</source>
        <translation>Melding</translation>
    </message>
    <message>
        <location filename="src/MessageModel.cpp" line="382"/>
        <source>Time</source>
        <translation>Tijd</translation>
    </message>
    <message>
        <location filename="src/MessageModel.cpp" line="385"/>
        <source>Reset</source>
        <translation>Gekwiteerd</translation>
    </message>
</context>
</TS>
