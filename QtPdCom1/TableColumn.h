/*****************************************************************************
 *
 * Copyright (C) 2012-2022  Florian Pose <fp@igh.de>
 *                    2013  Dr. Wilhelm Hagemeister <hm@igh-essen.com>
 *
 * This file is part of the QtPdCom library.
 *
 * The QtPdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The QtPdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdCom Library. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef QTPDCOM_TABLECOLUMN_H
#define QTPDCOM_TABLECOLUMN_H

#include "Transmission.h"
#include "Export.h"

#include <pdcom5/Process.h>

#include <QColor>
#include <QObject>
#include <QString>

namespace QtPdCom {

/****************************************************************************/

/** Table Column.
 */
class QTPDCOM_PUBLIC TableColumn:
    public QObject
{
    Q_OBJECT

    public:
        TableColumn(const QString & = QString(), QObject *parent = nullptr);
        ~TableColumn();

        void setHeader(const QString &);
        const QString &getHeader() const;

        void setVariable(PdCom::Variable,
                const Transmission & = QtPdCom::event_mode, double = 1.0,
                double = 0.0);
        void setVariable(PdCom::Process *, const QString &,
                const Transmission & = QtPdCom::event_mode,
                double = 1.0, double = 0.0);
        void clearVariable();
        void clearData();

        quint32 getDecimals() const;
        void setDecimals(quint32);

        unsigned int getRows() const;

        QVariant data(unsigned int, int) const;
        QVariant headerData(int) const;
        Qt::ItemFlags flags(unsigned int) const;
        bool setData(unsigned int, const QString &, int);
        void setEnabled(bool, int = -1);

        bool isEditing() const;
        bool isEnabled() const;
        void commit();
        void revert();

        void setHighlightRow(int);

        void setHighlightColor(QColor);
        void setDisabledColor(QColor);

        /** Exception type.
         */
        struct Exception {
            /** Constructor.
             */
            Exception(const QString &msg): msg(msg) {}
            QString msg; /**< Exception message. */
        };

    signals:
        void dimensionChanged();
        void headerChanged();
        void valueChanged();

    private:
        class Q_DECL_HIDDEN Impl;
        std::unique_ptr<Impl> impl;
};

/****************************************************************************/

} // namespace

#endif
