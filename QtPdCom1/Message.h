/*****************************************************************************
 *
 * Copyright (C) 2009-2023  Florian Pose <fp@igh.de>
 *
 * This file is part of the QtPdCom library.
 *
 * The QtPdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The QtPdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdCom Library. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef QTPDCOM_MESSAGE_H
#define QTPDCOM_MESSAGE_H

#include "Export.h"

#include <QObject>

#include <memory>

namespace QtPdCom {

/****************************************************************************/

/** Process message.
 */
class QTPDCOM_PUBLIC Message:
    public QObject
{
    Q_OBJECT

    friend class MessageModel;

    public:
        /** Message type.
         */
        enum Type {
            Information, /**< Non-critical information. */
            Warning, /**< Warning, that does not influence
                       the process flow. */
            Error, /**< Error, that influences the process flow. */
            Critical /**< Critical error, that makes the process
                       unable to run. */
            };

        Message(QObject *parent = nullptr);
        ~Message();

        bool isActive() const;
        double getTime() const;
        Type getType() const;
        const QString &getPath() const;
        int getIndex() const;
        QString getText(const QString & = QString()) const;
        QString getDescription(const QString & = QString()) const;
        QString getTimeString() const;

        /** Exception type.
         */
        struct Exception {
            /** Constructor.
             */
            Exception(const QString &msg): msg(msg) {}
            QString msg; /**< Exception message. */
        };

    signals:
        void stateChanged();

    private:
        class Q_DECL_HIDDEN Impl;
        std::unique_ptr<Impl> impl;
};

/****************************************************************************/

} // namespace

#endif
