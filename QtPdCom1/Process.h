/*****************************************************************************
 *
 * Copyright (C) 2009-2022  Florian Pose <fp@igh.de>
 *
 * This file is part of the QtPdCom library.
 *
 * The QtPdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The QtPdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdCom Library. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef QTPDCOM_PROCESS_H
#define QTPDCOM_PROCESS_H

#include <QTcpSocket>

#include <pdcom5.h>
#include <pdcom5/Process.h>

#ifndef PDCOM_VERSION_CODE
# error "No PDCOM_VERSION_CODE found."
#elif \
    !PDCOM_DEVEL \
    && (PDCOM_VERSION_CODE < PDCOM_VERSION(5, 0, 0) \
    || PDCOM_VERSION_CODE >= PDCOM_VERSION(6, 0, 0))
# error "Invalid PdCom version."
#endif

#include "Export.h"

class QTranslator;

namespace QtPdCom {

/****************************************************************************/

/** PdCom::Process implementation for Qt.
 */
class QTPDCOM_PUBLIC Process:
    public QObject, public PdCom::Process
{
    Q_OBJECT

    public:
        Process(QObject *parent = nullptr);
        virtual ~Process();

        void setApplicationName(const QString &);
        void connectToHost(const QString &, quint16 = 2345);
        void disconnectFromHost();

        /** State of the process connection.
         */
        enum ConnectionState {
            Disconnected, /**< Process disconnected. */
            Connecting, /**< Currently connecting. */
            Connected, /**< Process connection established. */
            ConnectError, /**< An error happened while connecting. */
            ConnectedError /**< An error happened, after the connection was
                             established. */
        };
        ConnectionState getConnectionState() const;
        bool isConnected() const;
        const QString &getErrorString() const;
        QString getPeerName() const;

        void find(const QString &);
        void sendBroadcast(const QString &, const QString &attr = "text");
        quint64 getRxBytes() const;
        quint64 getTxBytes() const;

        static QtPdCom::Process *getDefaultProcess();
        static void setDefaultProcess(QtPdCom::Process *);
        static bool loadTranslations(QTranslator& translator, const QString &locale);

        PdCom::MessageManagerBase *getMessageManager() const;

        // make some protected interfaces public
        using PdCom::Process::ping;

    private:
        struct Q_DECL_HIDDEN Impl;
        std::unique_ptr<Impl> impl;

        /** Virtual from PdCom::Process. */
        std::string applicationName() const override;
        int read(char *, int) override;
        void write(const char *, size_t) override;
        void flush() override;
        void connected() override;
        void broadcastReply(
                const std::string &message,
                const std::string &attr,
                std::chrono::nanoseconds time_ns,
                const std::string &user) override;
        void pingReply() override;
        void reset();

        /** Disconnect method inherited from QObject.
         *
         * This is made private, to avoid confusion.
         */
        bool disconnect(
                const char *signal = 0, /**< Signal. */
                const QObject *receiver = 0, /**< Receiver. */
                const char *method = 0 /**< Method. */
                );

        // make setMessageManager() private
        using PdCom::Process::setMessageManager;

    signals:
        /** Connection established.
         *
         * This is emitted after the connection is established.
         */
        void processConnected();

        /** Disconnected gracefully.
         *
         * This is only emitted, after the user called disconnectFromHost().
         */
        void disconnected();

        /** Connection error.
         *
         * This is emitted after a connection error or when the connection was
         * closed due to a parser error.
         */
        void error();

        void broadcastReceived(
                const QString &message,
                const QString &attr,
                std::uint64_t time_ns,
                const QString &user);

        /** Received ping reply.
         *
         * This is emitted after the reply to a ping (sent with ping()) was
         * received from the server.
         */
        void pingReceived();

    private slots:
        void socketConnected();
        void socketDisconnected();
        void socketError();
        void socketRead();
};

/****************************************************************************/

} // namespace

#endif
