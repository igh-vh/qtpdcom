/*****************************************************************************
 *
 * Copyright (C) 2022  Florian Pose <fp@igh.de>
 *
 * This file is part of the QtPdCom library.
 *
 * The QtPdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The QtPdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdCom Library. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "MessageModelTest.h"

#include <QAbstractItemModelTester>

#include "MessageModel.h"
#include "Process.h"

#include "../src/MessageManager.h"

/****************************************************************************/

void MessageModelTest::test_plain_xml()
{
    QtPdCom::Process process;
    PdCom::Variable busMonitor0(&process, "/Io/BusMonitor/Error/time/0");
    PdCom::Variable busMonitor1(&process, "/Io/BusMonitor/Error/time/1");

    QtPdCom::MessageModel *model = new QtPdCom::MessageModel(this);

    new QAbstractItemModelTester(model,
            QAbstractItemModelTester::FailureReportingMode::Fatal, this);

    model->load("plainmessages.xml", "de");
    QCOMPARE(model->rowCount(QModelIndex()), 0);

    model->connect(&process);

    busMonitor0.mockValue(1.0);

    QCOMPARE(model->rowCount(QModelIndex()), 1);
    auto index = model->index(0, 0, QModelIndex());
    QCOMPARE(model->data(index, Qt::DisplayRole),
            "Feldbus gestört an Panel Gelb");
    QCOMPARE(model->flags(index), Qt::ItemIsEnabled);
    index = model->index(0, 1, QModelIndex());
    QCOMPARE(model->data(index, Qt::DisplayRole),
            "1970-01-01 01:00:01,000000");

    busMonitor1.mockValue(2.0);

    QCOMPARE(model->rowCount(QModelIndex()), 2);

    index = model->index(0, 0, QModelIndex());
    QCOMPARE(model->data(index, Qt::DisplayRole),
            "Feldbus gestört an Panel Grün");
    QCOMPARE(model->flags(index), Qt::ItemIsEnabled);
    index = model->index(0, 1, QModelIndex());
    QCOMPARE(model->data(index, Qt::DisplayRole),
            "1970-01-01 01:00:02,000000");

    index = model->index(1, 0, QModelIndex());
    QCOMPARE(model->data(index, Qt::DisplayRole),
            "Feldbus gestört an Panel Gelb");
    QCOMPARE(model->flags(index), Qt::ItemIsEnabled);
    index = model->index(1, 1, QModelIndex());
    QCOMPARE(model->data(index, Qt::DisplayRole),
            "1970-01-01 01:00:01,000000");

    busMonitor1.mockValue(0.0);

    index = model->index(0, 0, QModelIndex());
    QCOMPARE(model->data(index, Qt::DisplayRole),
            "Feldbus gestört an Panel Grün");
    QCOMPARE(model->flags(index), Qt::ItemFlags());
    index = model->index(0, 1, QModelIndex());
    QCOMPARE(model->data(index, Qt::DisplayRole),
            "1970-01-01 01:00:02,000000");

    index = model->index(1, 0, QModelIndex());
    QCOMPARE(model->data(index, Qt::DisplayRole),
            "Feldbus gestört an Panel Gelb");
    QCOMPARE(model->flags(index), Qt::ItemIsEnabled);
    index = model->index(1, 1, QModelIndex());
    QCOMPARE(model->data(index, Qt::DisplayRole),
            "1970-01-01 01:00:01,000000");

    busMonitor0.mockValue(0.0);

    index = model->index(0, 0, QModelIndex());
    QCOMPARE(model->data(index, Qt::DisplayRole),
            "Feldbus gestört an Panel Grün");
    QCOMPARE(model->flags(index), Qt::ItemFlags());
    index = model->index(0, 1, QModelIndex());
    QCOMPARE(model->data(index, Qt::DisplayRole),
            "1970-01-01 01:00:02,000000");

    index = model->index(1, 0, QModelIndex());
    QCOMPARE(model->data(index, Qt::DisplayRole),
            "Feldbus gestört an Panel Gelb");
    QCOMPARE(model->flags(index), Qt::ItemFlags());
    index = model->index(1, 1, QModelIndex());
    QCOMPARE(model->data(index, Qt::DisplayRole),
            "1970-01-01 01:00:01,000000");

    model->clear();
}

/****************************************************************************/

void MessageModelTest::test_message_manager()
{
    qDebug() << "message manager";

    QtPdCom::Process process;

    QtPdCom::MessageModel *model = new QtPdCom::MessageModel(this);
    new QAbstractItemModelTester(model,
            QAbstractItemModelTester::FailureReportingMode::Fatal, this);

    QCOMPARE(model->rowCount(QModelIndex()), 0);

    model->connect(&process);

    QModelIndex index;

    qDebug() << "throwing message";
    PdCom::Message msg1 = {
        1, // seqNo
        PdCom::LogLevel::Error, // logLevel
        "/Message1", // path
        std::chrono::nanoseconds{1000}, // time
        "Feldbus gestört an Panel Gelb", // text
        -1 // index
    };
    process.mockMessage(msg1);

    QCOMPARE(model->rowCount(QModelIndex()), 1);
    index = model->index(0, 0, QModelIndex());
    QCOMPARE(model->data(index, Qt::DisplayRole),
            "Feldbus gestört an Panel Gelb");
    QCOMPARE(model->flags(index), Qt::ItemIsEnabled);
    index = model->index(0, 1, QModelIndex());
    QCOMPARE(model->data(index, Qt::DisplayRole),
            "1970-01-01 01:00:00,000001");
    index = model->index(0, 2, QModelIndex());
    QCOMPARE(model->data(index, Qt::DisplayRole), "");

    qDebug() << "resetting message";
    PdCom::Message msg2 = {
        2, // seqNo
        PdCom::LogLevel::Reset, // logLevel
        "/Message1", // path
        std::chrono::nanoseconds{2000}, // time
        "", // text
        -1 // index
    };

    QSignalSpy spy(model, SIGNAL(dataChanged(QModelIndex, QModelIndex)));
    process.mockMessage(msg2);
    QCOMPARE(spy.count(), 1);
    QList<QVariant> arguments = spy.takeFirst();
    QCOMPARE(arguments[0].toModelIndex().row(), 0);
    QCOMPARE(arguments[0].toModelIndex().column(), 0);
    QCOMPARE(arguments[1].toModelIndex().row(), 0);
    QCOMPARE(arguments[1].toModelIndex().column(), 2);

    // message should stay in list, but gray
    QCOMPARE(model->rowCount(QModelIndex()), 1);
    index = model->index(0, 0, QModelIndex());
    QCOMPARE(model->data(index, Qt::DisplayRole),
            "Feldbus gestört an Panel Gelb");
    QCOMPARE(model->flags(index), Qt::ItemFlags());
    index = model->index(0, 1, QModelIndex());
    QCOMPARE(model->data(index, Qt::DisplayRole),
            "1970-01-01 01:00:00,000001");
    index = model->index(0, 2, QModelIndex());
    QCOMPARE(model->data(index, Qt::DisplayRole),
            "1970-01-01 01:00:00,000002");

    qDebug() << "throwing message again";
    PdCom::Message msg3 = {
        3, // seqNo
        PdCom::LogLevel::Error, // logLevel
        "/Message1", // path
        std::chrono::nanoseconds{3000}, // time
        "Feldbus gestört an Panel Gelb", // text
        -1 // index
    };
    process.mockMessage(msg3);

    // two messages, first enable, second disabled
    QCOMPARE(model->rowCount(QModelIndex()), 2);

    index = model->index(0, 0, QModelIndex());
    QCOMPARE(model->data(index, Qt::DisplayRole),
            "Feldbus gestört an Panel Gelb");
    QCOMPARE(model->flags(index), Qt::ItemIsEnabled);
    index = model->index(0, 1, QModelIndex());
    QCOMPARE(model->data(index, Qt::DisplayRole),
            "1970-01-01 01:00:00,000003");

    index = model->index(1, 0, QModelIndex());
    QCOMPARE(model->data(index, Qt::DisplayRole),
            "Feldbus gestört an Panel Gelb");
    QCOMPARE(model->flags(index), Qt::ItemFlags());
    index = model->index(1, 1, QModelIndex());
    QCOMPARE(model->data(index, Qt::DisplayRole),
            "1970-01-01 01:00:00,000001");

    model->clear();
}

/****************************************************************************/

void MessageModelTest::test_mixed()
{
    QtPdCom::Process process;
    PdCom::Variable busMonitor0(&process, "/Io/BusMonitor/Error/time/0");
    PdCom::Variable busMonitor1(&process, "/Io/BusMonitor/Error/time/1");

    QtPdCom::MessageModel model;
    model.load("plainmessages.xml", "de");
    QCOMPARE(model.rowCount(QModelIndex()), 0);

    new QAbstractItemModelTester(&model,
            QAbstractItemModelTester::FailureReportingMode::Fatal, this);

    model.connect(&process);

    qDebug() << "First message via variable";
    busMonitor0.mockValue(0.000001);

    QCOMPARE(model.rowCount(QModelIndex()), 1);

    auto index = model.index(0, 0, QModelIndex());
    QCOMPARE(model.data(index, Qt::DisplayRole),
            "Feldbus gestört an Panel Gelb");
    QCOMPARE(model.flags(index), Qt::ItemIsEnabled);
    index = model.index(0, 1, QModelIndex());
    QCOMPARE(model.data(index, Qt::DisplayRole),
            "1970-01-01 01:00:00,000001");

    qDebug() << "Same message via manager";
    PdCom::Message msg1 = {
        1, // seqNo
        PdCom::LogLevel::Error, // logLevel
        "/Io/BusMonitor/Error/time/0", // path
        std::chrono::nanoseconds{1000}, // time
        "Feldbus gestört an Panel Gelb", // text
        -1 // index
    };
    process.mockMessage(msg1);
    QCOMPARE(model.rowCount(QModelIndex()), 1);
    index = model.index(0, 0, QModelIndex());
    QCOMPARE(model.data(index, Qt::DisplayRole),
            "Feldbus gestört an Panel Gelb");
    QCOMPARE(model.flags(index), Qt::ItemIsEnabled);
    index = model.index(0, 1, QModelIndex());
    QCOMPARE(model.data(index, Qt::DisplayRole),
            "1970-01-01 01:00:00,000001");

    qDebug() << "Second message via manager";
    PdCom::Message msg3 = {
        2, // seqNo
        PdCom::LogLevel::Warn, // logLevel
        "/OtherMessage", // path
        std::chrono::nanoseconds{2000}, // time
        "Second Message", // text
        -1 // index
    };
    process.mockMessage(msg3);

    QCOMPARE(model.rowCount(QModelIndex()), 2);

    index = model.index(0, 0, QModelIndex());
    QCOMPARE(model.data(index, Qt::DisplayRole),
            "Feldbus gestört an Panel Gelb");
    QCOMPARE(model.flags(index), Qt::ItemIsEnabled);
    index = model.index(0, 1, QModelIndex());
    QCOMPARE(model.data(index, Qt::DisplayRole),
            "1970-01-01 01:00:00,000001");

    index = model.index(1, 0, QModelIndex());
    QCOMPARE(model.data(index, Qt::DisplayRole),
            "Second Message");
    QCOMPARE(model.flags(index), Qt::ItemIsEnabled);
    index = model.index(1, 1, QModelIndex());
    QCOMPARE(model.data(index, Qt::DisplayRole),
            "1970-01-01 01:00:00,000002");

    qDebug() << "Remove first message via manager";
    PdCom::Message msg2 = {
        3, // seqNo
        PdCom::LogLevel::Reset, // logLevel
        "/Io/BusMonitor/Error/time/0", // path
        std::chrono::nanoseconds{3000}, // time
        "Feldbus gestört an Panel Gelb", // text
        -1 // index
    };
    process.mockMessage(msg2);

    QCOMPARE(model.rowCount(QModelIndex()), 2);

    index = model.index(0, 0, QModelIndex());
    QCOMPARE(model.data(index, Qt::DisplayRole),
            "Feldbus gestört an Panel Gelb");
    QCOMPARE(model.flags(index), Qt::ItemFlags());
    index = model.index(0, 1, QModelIndex());
    QCOMPARE(model.data(index, Qt::DisplayRole),
            "1970-01-01 01:00:00,000001");

    index = model.index(1, 0, QModelIndex());
    QCOMPARE(model.data(index, Qt::DisplayRole),
            "Second Message");
    QCOMPARE(model.flags(index), Qt::ItemIsEnabled);
    index = model.index(1, 1, QModelIndex());
    QCOMPARE(model.data(index, Qt::DisplayRole),
            "1970-01-01 01:00:00,000002");
}

/****************************************************************************/

void MessageModelTest::test_history()
{
    qDebug() << "history";

    QtPdCom::Process process;
    process.mockHistoricMessage({
            1, // seqNo
            PdCom::LogLevel::Error, // logLevel
            "/Limit", // path
            std::chrono::nanoseconds{1000}, // time
            "Event message 1", // text
            0 // index
            });
    process.mockHistoricMessage({
            2, // seqNo
            PdCom::LogLevel::Reset, // logLevel
            "/Limit", // path
            std::chrono::nanoseconds{2000}, // time
            "", // text
            0 // index
            });
    process.mockHistoricMessage({
            3, // seqNo
            PdCom::LogLevel::Error, // logLevel
            "/Limit", // path
            std::chrono::nanoseconds{3000}, // time
            "Event message 2", // text
            1 // index
            });

    QtPdCom::MessageModel model;
    QCOMPARE(model.rowCount(QModelIndex()), 0);

    new QAbstractItemModelTester(&model,
            QAbstractItemModelTester::FailureReportingMode::Fatal, this);

    model.connect(&process);
    process.mockConnected();

    QModelIndex index;

    // processes events and tries again if necessary (for max. 5s)
    QTRY_COMPARE(model.rowCount(QModelIndex()), 2);

    index = model.index(0, 0, QModelIndex());
    QCOMPARE(model.data(index, Qt::DisplayRole),
            "Event message 2");
    QCOMPARE(model.flags(index), Qt::ItemIsEnabled);
    index = model.index(0, 1, QModelIndex());
    QCOMPARE(model.data(index, Qt::DisplayRole),
            "1970-01-01 01:00:00,000003");

    index = model.index(1, 0, QModelIndex());
    QCOMPARE(model.data(index, Qt::DisplayRole),
            "Event message 1");
    QCOMPARE(model.flags(index), Qt::ItemFlags());
    index = model.index(1, 1, QModelIndex());
    QCOMPARE(model.data(index, Qt::DisplayRole),
            "1970-01-01 01:00:00,000001");
}

/****************************************************************************/

void MessageModelTest::test_all_historic()
{
    qDebug() << "all historic";

    QtPdCom::Process process;
    process.mockHistoricMessage({
            1, // seqNo
            PdCom::LogLevel::Error, // logLevel
            "/Limit", // path
            std::chrono::nanoseconds{1000}, // time
            "Event message 1", // text
            0 // index
            });
    process.mockHistoricMessage({
            2, // seqNo
            PdCom::LogLevel::Reset, // logLevel
            "/Limit", // path
            std::chrono::nanoseconds{2000}, // time
            "", // text
            0 // index
            });

    QtPdCom::MessageModel model;
    QCOMPARE(model.rowCount(QModelIndex()), 0);

    new QAbstractItemModelTester(&model,
            QAbstractItemModelTester::FailureReportingMode::Fatal, this);

    model.connect(&process);
    process.mockConnected();

    QModelIndex index;

    QTRY_COMPARE(model.rowCount(QModelIndex()), 1);

    index = model.index(0, 0, QModelIndex());
    QCOMPARE(model.data(index, Qt::DisplayRole),
            "Event message 1");
    QCOMPARE(model.flags(index), Qt::ItemFlags());
    index = model.index(0, 1, QModelIndex());
    QCOMPARE(model.data(index, Qt::DisplayRole),
            "1970-01-01 01:00:00,000001");
}

/****************************************************************************/

void MessageModelTest::test_can_fetch_more()
{
    qDebug() << "can fetch more";

    QtPdCom::Process process;

    for (uint32_t i = 1; i < 60; i += 2) {
        process.mockHistoricMessage({
                i, // seqNo
                PdCom::LogLevel::Error, // logLevel
                "/Limit", // path
                std::chrono::nanoseconds{
                    (uint64_t) i * 1000000000}, // time
                "Event message 1", // text
                0 // index
                });
        process.mockHistoricMessage({
                i + 1, // seqNo
                PdCom::LogLevel::Reset, // logLevel
                "/Limit", // path
                std::chrono::nanoseconds{
                    (uint64_t) i * 1000000000 + 1000}, // time
                "", // text
                0 // index
                });
    }

    QtPdCom::MessageModel model;
    QCOMPARE(model.rowCount(QModelIndex()), 0);

    model.connect(&process);
    process.mockConnected();

    QModelIndex index;

    QTRY_COMPARE(model.rowCount(QModelIndex()), 1);

    index = model.index(0, 0, QModelIndex());
    QCOMPARE(model.data(index, Qt::DisplayRole),
            "Event message 1");
    QCOMPARE(model.flags(index), Qt::ItemFlags());
    index = model.index(0, 1, QModelIndex());
    QCOMPARE(model.data(index, Qt::DisplayRole),
            "1970-01-01 01:00:59,000000");
    index = model.index(0, 2, QModelIndex());
    QCOMPARE(model.data(index, Qt::DisplayRole),
            "1970-01-01 01:00:59,000001");

    QCOMPARE(model.canFetchMore(QModelIndex()), true);
    model.fetchMore(QModelIndex());
    QTRY_COMPARE(model.rowCount(QModelIndex()), 2);

    index = model.index(1, 0, QModelIndex());
    QCOMPARE(model.data(index, Qt::DisplayRole),
            "Event message 1");
    QCOMPARE(model.flags(index), Qt::ItemFlags());
    index = model.index(1, 1, QModelIndex());
    QCOMPARE(model.data(index, Qt::DisplayRole),
            "1970-01-01 01:00:57,000000");
    index = model.index(1, 2, QModelIndex());
    QCOMPARE(model.data(index, Qt::DisplayRole),
            "1970-01-01 01:00:57,000001");

    QCOMPARE(model.canFetchMore(QModelIndex()), true);
    model.fetchMore(QModelIndex());
    QTRY_COMPARE(model.rowCount(QModelIndex()), 3);
}

/****************************************************************************/

void MessageModelTest::test_history_then_live()
{
    qDebug() << "history_then_live";

    QtPdCom::Process process;

    process.mockHistoricMessage({
            0, // seqNo
            PdCom::LogLevel::Error, // logLevel
            "/Limit", // path
            std::chrono::nanoseconds{
            (uint64_t) 0 * 1000000000}, // time
            "Event message 1", // text
            0 // index
            });
    process.mockHistoricMessage({
            1, // seqNo
            PdCom::LogLevel::Reset, // logLevel
            "/Limit", // path
            std::chrono::nanoseconds{
            (uint64_t) 0 * 1000000000 + 1000}, // time
            "", // text
            0 // index
            });

    QtPdCom::MessageModel model;
    QCOMPARE(model.rowCount(QModelIndex()), 0);

    model.connect(&process);
    process.mockConnected();

    QModelIndex index;

    QTRY_COMPARE(model.rowCount(QModelIndex()), 1);

    index = model.index(0, 0, QModelIndex());
    QCOMPARE(model.data(index, Qt::DisplayRole),
            "Event message 1");
    QCOMPARE(model.flags(index), Qt::ItemFlags());
    index = model.index(0, 1, QModelIndex());
    QCOMPARE(model.data(index, Qt::DisplayRole),
            "1970-01-01 01:00:00,000000");
    index = model.index(0, 2, QModelIndex());
    QCOMPARE(model.data(index, Qt::DisplayRole),
            "1970-01-01 01:00:00,000001");

    // now throw new message
    qDebug() << "throwing message";
    PdCom::Message msg1 = {
        2, // seqNo
        PdCom::LogLevel::Error, // logLevel
        "/Limit", // path
        std::chrono::nanoseconds{2000}, // time
        "Event message 1", // text
        0 // index
    };
    process.mockMessage(msg1);

    QTRY_COMPARE(model.rowCount(QModelIndex()), 2);

    index = model.index(0, 0, QModelIndex());
    QCOMPARE(model.data(index, Qt::DisplayRole),
            "Event message 1");
    QCOMPARE(model.flags(index), Qt::ItemIsEnabled);
    index = model.index(0, 1, QModelIndex());
    QCOMPARE(model.data(index, Qt::DisplayRole),
            "1970-01-01 01:00:00,000002");
    index = model.index(0, 2, QModelIndex());
    QCOMPARE(model.data(index, Qt::DisplayRole), "");

    index = model.index(1, 0, QModelIndex());
    QCOMPARE(model.data(index, Qt::DisplayRole),
            "Event message 1");
    QCOMPARE(model.flags(index), Qt::ItemFlags());
    index = model.index(1, 1, QModelIndex());
    QCOMPARE(model.data(index, Qt::DisplayRole),
            "1970-01-01 01:00:00,000000");
    index = model.index(1, 2, QModelIndex());
    QCOMPARE(model.data(index, Qt::DisplayRole),
            "1970-01-01 01:00:00,000001");

    // and reset the new message again
    qDebug() << "resetting message";
    PdCom::Message msg2 = {
        3, // seqNo
        PdCom::LogLevel::Reset, // logLevel
        "/Limit", // path
        std::chrono::nanoseconds{3000}, // time
        "", // text
        0 // index
    };
    process.mockMessage(msg2);

    QTRY_COMPARE(model.rowCount(QModelIndex()), 2);

    index = model.index(0, 0, QModelIndex());
    QCOMPARE(model.data(index, Qt::DisplayRole),
            "Event message 1");
    QCOMPARE(model.flags(index), Qt::ItemFlags());
    index = model.index(0, 1, QModelIndex());
    QCOMPARE(model.data(index, Qt::DisplayRole),
            "1970-01-01 01:00:00,000002");
    index = model.index(0, 2, QModelIndex());
    QCOMPARE(model.data(index, Qt::DisplayRole),
            "1970-01-01 01:00:00,000003");

    index = model.index(1, 0, QModelIndex());
    QCOMPARE(model.data(index, Qt::DisplayRole),
            "Event message 1");
    QCOMPARE(model.flags(index), Qt::ItemFlags());
    index = model.index(1, 1, QModelIndex());
    QCOMPARE(model.data(index, Qt::DisplayRole),
            "1970-01-01 01:00:00,000000");
    index = model.index(1, 2, QModelIndex());
    QCOMPARE(model.data(index, Qt::DisplayRole),
            "1970-01-01 01:00:00,000001");
}

/****************************************************************************/

void MessageModelTest::test_sorting()
{
    qDebug() << "sorting";

    QtPdCom::Process process;
    QtPdCom::MessageModel model;

    model.connect(&process);
    process.mockConnected();

    // throw warning
    qDebug() << "throwing warning";
    PdCom::Message msg1 = {
        0, // seqNo
        PdCom::LogLevel::Warn, // logLevel
        "/Warning", // path
        std::chrono::nanoseconds{1000}, // time
        "Warning message", // text
        -1 // index
    };
    process.mockMessage(msg1);

    QTRY_COMPARE(model.rowCount(QModelIndex()), 1);

    QModelIndex index;

    index = model.index(0, 0, QModelIndex());
    QCOMPARE(model.data(index, Qt::DisplayRole),
            "Warning message");
    QCOMPARE(model.flags(index), Qt::ItemIsEnabled);
    index = model.index(0, 1, QModelIndex());
    QCOMPARE(model.data(index, Qt::DisplayRole),
            "1970-01-01 01:00:00,000001");
    index = model.index(0, 2, QModelIndex());
    QCOMPARE(model.data(index, Qt::DisplayRole), "");

    // then throw error message
    qDebug() << "throwing error";
    PdCom::Message msg2 = {
        1, // seqNo
        PdCom::LogLevel::Error, // logLevel
        "/Error", // path
        std::chrono::nanoseconds{2000}, // time
        "Error message", // text
        -1 // index
    };
    process.mockMessage(msg2);

    QTRY_COMPARE(model.rowCount(QModelIndex()), 2);

    index = model.index(0, 0, QModelIndex());
    QCOMPARE(model.data(index, Qt::DisplayRole),
            "Error message");
    QCOMPARE(model.flags(index), Qt::ItemIsEnabled);
    index = model.index(0, 1, QModelIndex());
    QCOMPARE(model.data(index, Qt::DisplayRole),
            "1970-01-01 01:00:00,000002");
    index = model.index(0, 2, QModelIndex());
    QCOMPARE(model.data(index, Qt::DisplayRole),
            "");

    index = model.index(1, 0, QModelIndex());
    QCOMPARE(model.data(index, Qt::DisplayRole),
            "Warning message");
    QCOMPARE(model.flags(index), Qt::ItemIsEnabled);
    index = model.index(1, 1, QModelIndex());
    QCOMPARE(model.data(index, Qt::DisplayRole),
            "1970-01-01 01:00:00,000001");
    index = model.index(1, 2, QModelIndex());
    QCOMPARE(model.data(index, Qt::DisplayRole),
            "");
}

/****************************************************************************/

QTEST_GUILESS_MAIN(MessageModelTest)
