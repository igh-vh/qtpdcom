/*****************************************************************************
 *
 * Copyright (C) 2021 Richard Hacker (lerichi at gmx dot net),
 *                    Florian Pose (fp at igh dot de),
 *                    Bjarne von Horn (vh at igh dot de).
 *
 * This file is part of the QtPdCom library.
 *
 * The QtPdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The QtPdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdCom Library. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

/** @file */

#ifndef PDCOM5_PROCESS_H
#define PDCOM5_PROCESS_H

#include <string>
#include <vector>
#include <chrono>
#include <list>

#include "Variable.h"

/****************************************************************************/

namespace PdCom {

class MessageManagerBase;
class Message;

class Subscription;

class Process
{
    friend class MessageManagerBase;

    public:
        Process();
        ~Process();

        void reset();
        void asyncData();
        bool list(const std::string &path = "");
        bool find(const std::string &path);
        void setMessageManager(MessageManagerBase *);
        void broadcast(const std::string &message,
                const std::string &attr = "text");
        void ping();

        // mocking methods
        void addVariable(Variable *);
        Variable *getVariable(const std::string &path);
        void addSubscription(Subscription *);
        void mockValue(Variable *);
        void mockMessage(const PdCom::Message &message);
        void mockHistoricMessage(const PdCom::Message &message);
        void mockConnected();

    private:
        std::list<Variable *> variables_;
        std::list<Subscription *> subscriptions_;
        PdCom::MessageManagerBase *messageManager;
        std::list<PdCom::Message> messageHistory;

        virtual void listReply(std::vector<Variable> variables,
                std::vector<std::string> dirs);
        virtual void findReply(const Variable &variable);
        virtual std::string applicationName() const;
        virtual int read(char *, int);
        virtual void write(const char *, size_t);
        virtual void flush();
        virtual void connected();
        virtual void broadcastReply(
                const std::string &message,
                const std::string &attr,
                std::chrono::nanoseconds time_ns,
                const std::string &user);
        virtual void pingReply();
        void sendActiveMessagesReply();
        void sendMessage(uint32_t);
};

}  // namespace PdCom

/****************************************************************************/

#endif
