/*****************************************************************************
 *
 * Copyright (C) 2021 Richard Hacker (lerichi at gmx dot net),
 *                    Florian Pose (fp at igh dot de),
 *                    Bjarne von Horn (vh at igh dot de).
 *
 * This file is part of the QtPdCom library.
 *
 * The QtPdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The QtPdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdCom Library. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

/** @file */

#ifndef PDCOM5_SIZETYPEINFO_H
#define PDCOM5_SIZETYPEINFO_H

/****************************************************************************/

namespace PdCom {

struct TypeInfo
{
    enum DataType {
        DataTypeBegin = 0,
        boolean_T     = DataTypeBegin,
        uint8_T,
        int8_T,
        uint16_T,
        int16_T,
        uint32_T,
        int32_T,
        uint64_T,
        int64_T,
        double_T,
        single_T,
        char_T,
        DataTypeEnd  // keep this as the very last one!
    };

    DataType type;
    /** name of the correspondig c type (double, char, ...) */
    const char *ctype;
    /** Size of one element in bytes */
    size_t element_size;
};

namespace details {

    template <typename T>
        struct TypeInfoTraits
        {};

    template <>
        struct TypeInfoTraits<bool>
        {
            static constexpr TypeInfo type_info = {
                TypeInfo::boolean_T, "bool", sizeof(bool)};
        };

    template <>
        struct TypeInfoTraits<char>
        {
            static constexpr TypeInfo type_info = {
                TypeInfo::char_T, "char", sizeof(char)};
        };

    template <>
        struct TypeInfoTraits<uint8_t>
        {
            static constexpr TypeInfo type_info = {
                TypeInfo::uint8_T, "uint8_t", sizeof(uint8_t)};
        };
    template <>
        struct TypeInfoTraits<int8_t>
        {
            static constexpr TypeInfo type_info = {
                TypeInfo::int8_T, "int8_t", sizeof(int8_t)};
        };
    template <>
        struct TypeInfoTraits<uint16_t>
        {
            static constexpr TypeInfo type_info = {
                TypeInfo::uint16_T, "uint16_t", sizeof(uint16_t)};
        };
    template <>
        struct TypeInfoTraits<int16_t>
        {
            static constexpr TypeInfo type_info = {
                TypeInfo::int16_T, "int16_t", sizeof(int16_t)};
        };
    template <>
        struct TypeInfoTraits<uint32_t>
        {
            static constexpr TypeInfo type_info = {
                TypeInfo::uint32_T, "uint32_t", sizeof(uint32_t)};
        };
    template <>
        struct TypeInfoTraits<int32_t>
        {
            static constexpr TypeInfo type_info = {
                TypeInfo::int32_T, "int32_t", sizeof(int32_t)};
        };
    template <>
        struct TypeInfoTraits<uint64_t>
        {
            static constexpr TypeInfo type_info = {
                TypeInfo::uint64_T, "uint64_t", sizeof(uint64_t)};
        };
    template <>
        struct TypeInfoTraits<int64_t>
        {
            static constexpr TypeInfo type_info = {
                TypeInfo::int64_T, "int64_t", sizeof(int64_t)};
        };
    template <>
        struct TypeInfoTraits<double>
        {
            static constexpr TypeInfo type_info = {
                TypeInfo::double_T, "double", sizeof(double)};
        };
    template <>
        struct TypeInfoTraits<float>
        {
            static constexpr TypeInfo type_info = {
                TypeInfo::single_T, "float", sizeof(float)};
        };

    void copyData(
            void *dst,
            TypeInfo::DataType dst_type,
            const void *src,
            TypeInfo::DataType src_type,
            size_t nelem,
            size_t offset = 0);
}

} // namespace PdCom

/****************************************************************************/

#endif
