/*****************************************************************************
 *
 * Copyright (C) 2022 Florian Pose (fp at igh dot de),
 *
 * This file is part of the QtPdCom library.
 *
 * The QtPdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The QtPdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdCom Library. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "Process.h"

#include "Subscription.h"
#include "MessageManagerBase.h"

#include <QDebug>

using namespace PdCom;

/****************************************************************************/

Process::Process():
    messageManager{nullptr}
{
}

/****************************************************************************/

Process::~Process()
{
    if (messageManager) {
        messageManager->process = nullptr;
    }
}

/****************************************************************************/

void Process::reset()
{
}

/****************************************************************************/

void Process::asyncData()
{
}

/****************************************************************************/

bool Process::list(const std::string &)
{
    return false;
}

/****************************************************************************/

bool Process::find(const std::string &)
{
    return false;
}

/****************************************************************************/

void Process::setMessageManager(MessageManagerBase *m)
{
    if (messageManager) {
        messageManager->process = nullptr;
    }

    messageManager = m;

    if (m) {
        m->process = this;
    }
}

/****************************************************************************/

void Process::broadcast(const std::string &message,
        const std::string &attr)
{
    broadcastReply(message, attr, std::chrono::nanoseconds{0}, "testuser");
}

/****************************************************************************/

void Process::ping()
{
    pingReply();
}

/*****************************************************************************
 * Mock methods
 ****************************************************************************/

void Process::addVariable(Variable *variable)
{
    qDebug() << "Process" << this << "adding variable" << variable
        << "with path" << variable->getPath().c_str();
    variables_.push_back(variable);
}

/****************************************************************************/

Variable *Process::getVariable(const std::string &path)
{
    for (auto var : variables_) {
        if (var->getPath() == path) {
            return var;
        }
    }
    return nullptr;
}

/****************************************************************************/

void Process::addSubscription(Subscription *subscription)
{
    qDebug() << "Process" << this << "adding subscription" << subscription;
    subscriptions_.push_back(subscription);
}

/****************************************************************************/

void Process::mockValue(Variable *variable)
{
    // find subscriptions
    for (auto sub : subscriptions_) {
        if (sub->getVariablePtr() == variable) {
            sub->mockValue();
        }
    }
}

/****************************************************************************/

void Process::mockMessage(const PdCom::Message &message)
{
    if (!messageManager) {
        return;
    }

    messageManager->processMessage(message);
}

/****************************************************************************/

void Process::mockHistoricMessage(const PdCom::Message &message)
{
    messageHistory.push_back(message);
}

/****************************************************************************/

void Process::mockConnected()
{
    connected();
}

/*****************************************************************************
 * private methods
 ****************************************************************************/

void Process::listReply(std::vector<Variable>, std::vector<std::string>)
{
}

/****************************************************************************/

void Process::findReply(const Variable &)
{
}

/****************************************************************************/

std::string Process::applicationName() const
{
    return "PdCom Mock";
}

/****************************************************************************/

int Process::read(char *, int)
{
    return 0;
}

/****************************************************************************/

void Process::write(const char *, size_t)
{
}

/****************************************************************************/

void Process::flush()
{
}

/****************************************************************************/

void Process::connected()
{
    qWarning() << __func__ << "() not implemented";
}

/****************************************************************************/

void Process::broadcastReply(
        const std::string &,
        const std::string &,
        std::chrono::nanoseconds,
        const std::string &)
{
}

/****************************************************************************/

void Process::pingReply()
{
}

/****************************************************************************/

void Process::sendActiveMessagesReply()
{
    if (!messageManager) {
        qWarning() << __func__ << "no message manager!";
        return;
    }

    std::vector<PdCom::Message> ret;

    // find active messages (that have not been reset)
    for (auto i = messageHistory.begin(); i != messageHistory.end(); i++) {
        if (i->level != PdCom::LogLevel::Reset) {
            bool found{false};
            for (auto j : ret) {
                if (i->path == j.path and i->index == j.index) {
                    found = true;
                    break;
                }
            }
            if (not found) {
                ret.push_back(*i);
            }
        }
        else {
            for (std::vector<PdCom::Message>::const_iterator j = ret.begin();
                    j != ret.end(); j++) {
                if (i->path == j->path and i->index == j->index) {
                    ret.erase(j);
                    break;
                }
            }
        }
    }

    if (!ret.size() and messageHistory.size()) {
        ret.push_back(messageHistory.back());
    }

    qDebug() << __func__ << "len" << ret.size();
    for (auto i : ret) {
        qDebug() << __func__ << i.seqNo << i.path.c_str() << i.index
            << (int) i.level;
    }

    messageManager->activeMessagesReply(ret);
}

/****************************************************************************/

void Process::sendMessage(uint32_t seqNo)
{
    if (!messageManager) {
        qWarning() << __func__ << "no message manager!";
        return;
    }

    for (auto i = messageHistory.begin(); i != messageHistory.end(); i++) {
        if (i->seqNo == seqNo) {
            qDebug() << __func__ << i->seqNo << i->path.c_str() << i->index
                << (int) i->level;
            messageManager->getMessageReply(*i);
            return;
        }
    }

    // not found, answer with empty path
    messageManager->getMessageReply({
            seqNo, // seqNo
            PdCom::LogLevel::Reset, // logLevel
            "", // path
            std::chrono::nanoseconds{0}, // time
            "", // text
            -1 // index
            });
}

/****************************************************************************/
