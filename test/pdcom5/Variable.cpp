/*****************************************************************************
 *
 * Copyright (C) 2022 Florian Pose (fp@igh.de)
 *
 * This file is part of the QtPdCom library.
 *
 * The QtPdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The QtPdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdCom Library. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "Variable.h"

#include "Process.h"

#include <QDebug>

using namespace PdCom;

/****************************************************************************/

Variable::Variable():
    empty_{true},
    process_{nullptr},
    type_info_{TypeInfo::double_T, "double", sizeof(double)},
    path_{"/path"},
    mocked_value_{0.0}
{
}

/****************************************************************************/

void details::copyData(
        void *dst,
        TypeInfo::DataType dst_type,
        const void *src,
        TypeInfo::DataType src_type,
        size_t nelem,
        size_t offset)
{
    qDebug() << "Copying data from " << src << "to" << dst
        << "srctype" << src_type << "dsttype" << dst_type << "nelem" << nelem
        << "offset" << offset;

    if (nelem != 1 and offset != 0) {
        qWarning() << "not implemented!";
        return;
    }

    if (src_type == dst_type and src_type == TypeInfo::double_T) {
        memcpy(dst, src, nelem * sizeof(double));
        return;
    }

    qWarning() << "data types not implemented";
}

/*****************************************************************************
 * mocking interface
 ****************************************************************************/

Variable::Variable(Process *process, const std::string &path):
    empty_{false},
    process_{process},
    type_info_{TypeInfo::double_T, "double", sizeof(double)},
    path_{path},
    mocked_value_{0.0}
{
    process->addVariable(this);
}

/****************************************************************************/

void Variable::mockValue(double value)
{
    qDebug() << "Variable" << path_.c_str() << " mocking value " << value;
    mocked_value_ = value;
    process_->mockValue(this);
}

/****************************************************************************/
