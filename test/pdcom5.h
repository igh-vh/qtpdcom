/*****************************************************************************
 * vim:tw=78
 *
 * Copyright (C) 2015-2016  Richard Hacker (lerichi at gmx dot net)
 *                          Florian Pose <fp@igh.de>
 *
 * This file is part of the PdCom library.
 *
 * The PdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The PdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

/** @file */

#ifndef PDCOM5_H
#define PDCOM5_H


/// PdCom major version
#define PDCOM_MAJOR 5
/// PdCom minor version
#define PDCOM_MINOR 0
/// Patch level of release. Note this may be a string
#define PDCOM_RELEASE 0

/// Macro to generate a version code for comparison
#define PDCOM_VERSION(A, B, C) (((A) << 16) + ((B) << 8) + (C))

/// Current PdCom version
#define PDCOM_VERSION_CODE \
        PDCOM_VERSION(PDCOM_MAJOR, PDCOM_MINOR, PDCOM_RELEASE)

/// library version string as "major.minor.patch"
namespace PdCom {
extern const char* pdcom_version_string;
}

/** \mainpage
 *
 * \section intro Introduction.
 *
 * The <a href="http://etherlab.org/en/pdcom">PdCom library</a> provides a
 * flexible C++ API for non-realtime exchange of process data and is part of
 * the <a href="http://etherlab.org/en">EtherLab project</a>.
 * Python bindings are also available.
 *
 * The library relies heavily on C++'s subclassing where virtual methods are
 * reimplemented to communicate with the implementation.
 *
 * The library is absolutely free of system calls, most notably `read()`,
 * `write()` and `flush()`. These are pure virtual methods that must be
 * implemented to send and receive data from the server process.
 *
 * The library does not provide authentication itself, but provides methods to
 * support its implementation. The server performs authentication negotiation
 * using to the SASL standard.
 *
 * Other than libexpat, the library has no other dependencies.
 *
 * Due to the fact that the library does not require system calls, it can
 * easily be used on platforms other than Linux.
 *
 * A POSIX implementation for the abstract read(), write() and flush() is provided,
 * also a default (network implementation unaware) TLS provider.
 * The SimpleLoginManager is an easy-to-use wrapper for the Cyrus SASL library.
 *
 * \section license License
 *
 * The PdCom library is released under the terms and conditions of the <a
 * href="http://www.gnu.org/licenses/lgpl.html">GNU Lesser General Public
 * License (LGPL)</a>, version 3 or (at your option) any later version.
 *
 * \section protocols Implemented communication protocols
 *
 * The library provides an abstraction layer for process data communication
 * protocols together with implementations for these protocols. The protocols
 * currently implemented are:
 *
 * - The MSR protocol from Ingenieurgemeinschaft IgH (http://etherlab.org).
 *
 * Other protocols can be easily added.
 *
 * \section start Getting started
 *
 * The basic approach to using the library is to subclass PdCom::Process and
 * reimplement at least \ref PdCom::Process::read "read()", \ref
 * PdCom::Process::write "write()" and \ref PdCom::Process::flush "flush()".
 * Then initiate the (network) communication channel to the server.  Whenever
 * data arrives from the server, call \ref PdCom::Process::asyncData
 * "asyncData()" which in turn calls \ref PdCom::Process::read "read()" to
 * fetch the data.
 *
 * Whenever data needs to be transferred to the server, the library calls
 * \ref PdCom::Process::write "write()" excessively. The communication channel
 * buffer only needs to be send upon reception of \ref PdCom::Process::flush
 * "flush()".
 *
 * \ref PdCom::Process "Process" provices methods to obtain information about
 * the process as well as discovering the variable tree.
 *
 * To be able to receive variable values or change parameters, you need to
 * subclass and instantiate an instance PdCom::Subscriber and reimplement its
 * pure virtual methods.
 *
 * Have a look at the class documentation to get more information. An example
 * is located in the example directory.
 *
 * The default POSIX Process implementation is called PdCom::PosixProcess,
 * the TLS-enabled Process is called PdCom::SecureProcess.
 * They can be used in case no framework (e.g. Qt) is available.
 * PdCom::SimpleLoginManager makes authentification with SASL a lot easier.
 *
 * One important thing: Make sure that every PdCom::Subscriber instance lives
 * longer than its Subscriptions. When a Subscription is destructed (or moved
 * from), the Subscriber will be accessed. So, do not delete a Subscriber
 * until every Subscription has gone.
 *
 * \section cmake CMake integration
 *
 * CMake modules are provided, too. Use the following to link your application
 * against pdcom:
 * \code
 * CMAKE_MINIMUM_REQUIRED(VERSION 3.3)
 * PROJECT(PdComExamples)
 *
 * FIND_PACKAGE(PdCom5 REQUIRED COMPONENTS gnutls)
 *
 * ADD_EXECUTABLE(app app.cpp)
 * TARGET_LINK_LIBRARIES(app PUBLIC EtherLab::pdcom5 EtherLab::pdcom5-gnutls)
 * \endcode
 *
 * \section further Further information
 *
 * There is a Qt-based widget library named <a
 * href="http://etherlab.org/en/pdwidgets">QtPdWidgets</a>, providing process
 * data visualisation and control widgets, that is based on PdCom.
 */

#endif // PDCOM5_H

/****************************************************************************/
