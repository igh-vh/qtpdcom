/*****************************************************************************
 *
 * Copyright (C) 2009-2022  Florian Pose <fp@igh.de>
 *
 * This file is part of the QtPdCom library.
 *
 * The QtPdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The QtPdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdCom Library. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "ui_MainWindow.h"

#include <QtPdCom1/Process.h>
#include <QtPdCom1/TableModel.h>
#include <QtPdCom1/MessageModel.h>

#include <QMainWindow>
#include <QTranslator>

/****************************************************************************/

class MainWindow:
    public QMainWindow,
    public Ui::MainWindow
{
    Q_OBJECT

    public:
        MainWindow(QTranslator& translator, QWidget * = 0);
        ~MainWindow();

    private:
        QtPdCom::Process p;
        QtPdCom::MessageModel messageModel;
        QTranslator& translator;

    private slots:
        void processConnected();
        void processDisconnected();
        void processError();
        void on_actionConnect_triggered();
        void on_actionDisconnect_triggered();
        void on_actionGerman_triggered();
        void on_actionEnglish_triggered();
        void on_actionDutch_triggered();
        void changeEvent(QEvent *event) override;
};

/****************************************************************************/
